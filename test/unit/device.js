"use strict";

var sensor = require('../../index')
  , events = require("events")
  , _ = require('lodash')

describe('Device class', function() {

  it('class should inherit from EventEmitter', function(done) {
    var device = new sensor.Device()

    device.should.be.instanceOf(events.EventEmitter)
    sensor.Device.super_.should.equal(events.EventEmitter)

    done()
  })

  it('Device class should have all methods', function(done) {
    var device = new sensor.Device()
    var methods = [
      'setAccelerometerConfig',
      'getAcceleration',
      'getSelfTestFactors',
      'runAccelerometerSelfTest',
      'wakeUp',
      'sleep',
    ]

    _.each(methods, function(method) {
      device.should.have.property(method)
      device[method].should.be.Function
    })

    done()
  })

})
