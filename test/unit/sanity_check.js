"use strict";

var sensor = require('../../')

describe('Sanity check', function() {

  it('shoud always pass', function(done) {
    true.should.equal(true)
    done()
  })

  it('sensor interface should return correct lib version', function(done) {

    var conf = require('../../package.json')
    conf.should.have.property('version')

    var version = sensor.getVersion()
    version.should.equal(conf.version)

    done()
  })

})
