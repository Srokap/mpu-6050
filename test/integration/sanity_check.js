"use strict";

var sensor = require('../../index')
  , Device = sensor.Device
  , I2C = require('i2c')
  , should  = require('should')
  , _ = require('lodash')
  //, async = require('async')

//require('coffee-trace')

describe('Connection check', function() {

  var deviceAddress1 = 0x68
  var deviceAddress2 = 0x69

  before(function(done) {

    sensor.scanDevices(1, function(err, devices) {
      if (!_.includes(devices, deviceAddress1)) {
        throw new Error('Device 1 not connected')
      }
      if (!_.includes(devices, deviceAddress2)) {
        throw new Error('Device 2 not connected')
      }
      done()
    })

  })

  it('shoud always pass', function(done) {
    true.should.equal(true)
    done()
  })

  it('should connect to device 1 directly using default address and bus', function(done) {

    var wire = new I2C(deviceAddress1, {
      device: '/dev/i2c-1'
    })

    wire.on('error', function(err) {
      should.not.exist(err)
    })

    should.exist(Device.reg.WHO_AM_I)

    // check the WHO_AM_I register
    wire.readBytes(Device.reg.WHO_AM_I, 1, function(err, buffer) {
      should.not.exist(err)

      buffer[0].should.equal(deviceAddress1)

      done()
    })
  })

  it('should connect to device 2 directly using default address and bus', function(done) {

    var wire = new I2C(deviceAddress2, {
      device: '/dev/i2c-1'
    })

    wire.on('error', function(err) {
      should.not.exist(err)
    })

    should.exist(Device.reg.WHO_AM_I)

    // check the WHO_AM_I register
    wire.readBytes(Device.reg.WHO_AM_I, 1, function(err, buffer) {
      should.not.exist(err)

      buffer[0].should.equal(deviceAddress1)

      done()
    })
  })

  it('should fail to use invalid device address', function(done) {

    var wire = new I2C(0x7F, {
      device: '/dev/i2c-1'
    })

    wire.on('error', function(err) {
      should.not.exist(err)
    })

    should.exist(Device.reg.WHO_AM_I)

    wire.readBytes(Device.reg.WHO_AM_I, 1, function(err) {
      should.exist(err)

      err.should.be.Error.and.eql(new Error('Error reading length of bytes'))

      done()
    })
  })
})
