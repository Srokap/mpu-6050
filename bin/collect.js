
"use strict";

var sensor = require('./../lib/index.js')
  , Device = sensor.Device
  , async = require('async')
  , deviceA = new Device({
    address: 0x68
  })
  , deviceB = new Device({
    address: 0x69
  })

var format = 'json'

process.argv.forEach(function(val, index) {
  if (val === '--csv') {
    format = 'csv'
  }
})


deviceA.connect(function() {
  deviceA.runAccelerometerSelfTest(function(err, isOk, res) {
    //if(!isOk) {
    //  console.error("Failed Accelerometer self test")
    //  process.exit(10)
    //}
    deviceA.setAccelerometerConfig(Device.const.AFS_SEL_16, false, function(err) {
      if(err) {
        console.error(err)
        process.exit(1)
      }

      deviceB.connect(function() {
        deviceB.runAccelerometerSelfTest(function(err, isOk, res) {
          //if(!isOk) {
          //  console.error("Failed Accelerometer self test")
          //  process.exit(11)
          //}
          deviceA.setAccelerometerConfig(Device.const.AFS_SEL_16, false, function(err) {

            if(err) {
              console.error(err)
              process.exit(2)
            }

            async.whilst(function() {
              return true
            }, function(callback) {
              var ts = Date.now()

              async.parallel([
                function(callback) {
                  deviceA.getAcceleration(function(err, resA) {
                    if(err) {
                      return callback(null, [null, null, null])
                      //return callback(err)
                    }

                    callback(null, resA)
                  })
                },
                function(callback) {
                  deviceA.getAcceleration(function(err, resB) {
                    if(err) {
                      return callback(null, [null, null, null])
                      //return callback(err)
                    }

                    callback(null, resB)
                  })
                },
              ], function(err, results) {
                if(err) {
                  console.error(err)
                  process.exit(4)
                }

                var row = [ts].concat(results[0]).concat(results[1])

                if(format === 'csv') {
                  process.stdout.write(row + "\r\n");
                } else {
                  console.log(JSON.stringify(row))
                }
                callback()
              })

            }, function(err) {
              if(err) {
                console.error(err)
                process.exit(3)
              }
              process.exit(0)
            })

          })
        })
      })
    })
  })
})

//10ms PLL settling time

