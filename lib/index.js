"use strict";

module.exports = (function() {

  return {

    Device: require('./device.js'),

    /**
     * Scan for devices on the bus. Result contains an array of addresses
     *
     * @param callback
     */
    scanDevices: function(busId, callback) {
      var I2C = require('i2c')

      var wire = new I2C(0x00, {
        device: '/dev/i2c-' + busId
      })

      wire.scan(function(err, addresses) {
        callback(err, addresses)
      })
    },

    /**
     * Returns version from package.json
     *
     * @returns string
     */
    getVersion: function() {
      var conf = require('../package.json')

      return conf.version
    }
  }
})()
